import BookService from './services/BookService';
const bookService = new BookService();

class UI {
    async renderBook() {
        const books = await bookService.getBooks();
        const booksCardContainer
    }

    async addNewBook(book) {
        await bookService.postBook(book);
        this.clearBookForm();
    }

    clearBookForm() {
        document.getElementById('book-form').reset();
    }

    renderBookForm() {

    }

    deleteBook() {

    }
}

export default UI;