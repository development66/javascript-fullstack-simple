class BookService {
    constructor() {
        this.URI = 'http://localhost:3000/api/books';
    }

    async getBooks() {
        const response = await fetch(this.URI);
        const books = response.json();
        return books;
    }

    async postBook(book) {
        const res = await fetch(this.URI, {
            method: 'POST',
            body: book
        });
        const newBook = await res.json();

    }

    async deleteBook(bookId) {
        const res = await fetch(`${this.URI}/${this.bookId}`, {
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'DELETE'
        });
        const book = await res.json();
    }
    
}

module.exports = BookService;